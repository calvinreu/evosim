package main

import "math"

func (creature *Creature) GetInput() []float64 {
	input := make([]float64, 0, InputSize)
	Xc := int(creature.x)
	Yc := int(creature.y)

	//get tile relative to
	for i := 0; i < 8; i++ {
		X := Xc+int(math.Cos((math.Pi/4)*float64(i)+creature.angle)*1.999)
        Y := Yc+int(math.Sin((math.Pi/4)*float64(i)+ creature.angle)*1.999)
		input = append(input, chart.At(X, Y).Food)
		input = append(input, chart.At(X, Y).CreatureC)
		if chart.At(X, Y).Water {
			input = append(input, 1)
		} else {
			input = append(input, -1)
		}
	}

	input = append(input, chart.At(int(creature.x), int(creature.y)).Food)
	input = append(input, chart.At(int(creature.x), int(creature.y)).CreatureC)
	if chart.At(int(creature.x), int(creature.y)).Water {
		input = append(input, 1)
	} else {
		input = append(input, -1)
	}
	input = append(input, creature.food)
	input = append(input, creature.age)

	return input
}
