package main

import (
	"fmt"
	"math"
)

func TestInput() {
	chart.Init(3, 3)
	var creature Creature
	creature.x = 2
	creature.y = 2
	creature.angle = math.Pi
	for i := 0; i < len(chart.Data); i++ {
		chart.Data[i].CreatureC = float64(i)
		chart.Data[i].Food = float64(i)
	}

	fmt.Println(creature.GetInput())
}
