package env

const (
	StartingFood              = 20
	RespawnFood               = 0.05
	MaximumFood               = 20
	WaterPropabilitty         = 0.02
	WaterPropabilittyAdjacent = 0.25
	WaterPropabilittyDecrease = 0.0000001
)
