package env

type Tile struct {
	Food, CreatureC float64
	Water           bool
}
