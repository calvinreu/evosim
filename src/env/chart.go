package env

import "math/rand"

type Chart struct {
	Data       []Tile
	QuickAcces []int
	W, H       int
}

func (chart *Chart) Init(x, y int) {
	waterPos := make([]int, 0, int(float64(x*y)*WaterPropabilitty))
	chart.Data = make([]Tile, x*y)
	chart.QuickAcces = make([]int, y)
	for i := 0; i < y; i++ {
		chart.QuickAcces[i] = i * x
	}
	chart.W = x
	chart.H = y

	for i := 0; i < len(chart.Data); i++ {
		if rand.Float64() > WaterPropabilitty {
			chart.Data[i].Food = StartingFood
		} else {
			chart.Data[i].Water = true
			waterPos = append(waterPos, i)
		}
	}

	W_P := WaterPropabilittyAdjacent
	for _, water := range waterPos {
		X, Y := chart.GetCoords(water)
		AdjacentWater(chart, X, Y, &W_P)
	}

	for i := 0; i < len(chart.Data); i++ {
		if chart.Data[i].Water {
			chart.Data[i].Food = 0
		}
	}
}

func AdjacentWater(chart *Chart, x, y int, W_P *float64) {
	if !chart.At(x, y).Water {
		*W_P -= WaterPropabilittyDecrease
	}
	chart.At(x, y).Water = true

	if rand.Float64() < *W_P {
		AdjacentWater(chart, x, y-1, W_P)
	}
	if rand.Float64() < *W_P {
		AdjacentWater(chart, x, y+1, W_P)
	}
	if rand.Float64() < *W_P {
		AdjacentWater(chart, x-1, y, W_P)
	}
	if rand.Float64() < *W_P {
		AdjacentWater(chart, x+1, y, W_P)
	}
}

func (chart *Chart) GetCoords(i int) (int, int) {
	y := i / chart.W
	x := i - chart.QuickAcces[y]

	return x, y
}

func (chart *Chart) At(x, y int) *Tile {
	Y := y - ((y / chart.H) * chart.H)
	X := x - ((x / chart.W) * chart.W)

	if X < 0 {
		X += chart.W
	}

	if Y < 0 {
		Y += chart.H
	}

	return &chart.Data[chart.QuickAcces[Y]+X]
}

func (chart *Chart) Refresh() {
	for i := 0; i < len(chart.Data); i++ {
		if !chart.Data[i].Water {
			chart.Data[i].Food += RespawnFood
			if chart.Data[i].Food > MaximumFood {
				chart.Data[i].Food = MaximumFood
			}
		}
	}
}
