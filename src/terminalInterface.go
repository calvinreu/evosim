package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func GetInputString(command chan string) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("[evosim]: ")
	text, _ := reader.ReadString('\n')
	text = strings.ReplaceAll(text, " ", "")
	command <- strings.ReplaceAll(text, "\n", "")
}
