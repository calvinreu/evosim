package info

import "fmt"

type Info struct {
	Ticks, CreatureCount uint
}

func (info *Info) Format() string {
	return "Ticks:" + fmt.Sprint(info.Ticks) + " CreatureCount:" + fmt.Sprint(info.CreatureCount)
}
