package main

import (
	"container/list"
	"flag"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/calvinreu/evosim/src/env"
	"gitlab.com/calvinreu/evosim/src/gui"
	"gitlab.com/calvinreu/evosim/src/info"
)

var DEBUG bool
var GuiEnabled bool
var chart env.Chart
var creatures list.List
var renderer gui.Renderer
var simInfo info.Info

func main() {
	var filename string
	rand.Seed(time.Now().UnixNano())

	chart.Init(MapWidth, MapHeight)

	flag.BoolVar(&DEBUG, "DEBUG", false, "Commandline info")
	flag.BoolVar(&GuiEnabled, "GUI", true, "enable graphics")
	flag.StringVar(&filename, "LOADFILE", "", "load file")
	flag.Parse()
	creatures.Init()

	if filename == "" {
		for i := 0; i < StartCreature; i++ {
			NewCreature()
		}
	} else {
		Load(filename)
	}

	if GuiEnabled {
		EnableGui()
	}
	Mainloop()

	renderer.Quit()
}

func EnableGui() {
	var err error
	renderer, err = gui.Init(chart, DEBUG)
	if err != nil {
		fmt.Println(err)
		return
	}
	for i := creatures.Front(); i != nil; i = i.Next() {
		if creature, ok := i.Value.(*Creature); ok {
			creature.guiInstance = renderer.AddCreature(&creature.x, &creature.y, &creature.angle)
		} else if i != nil {
			fmt.Println("creatures contains wrong type")
		}
	}
}
