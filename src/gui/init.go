package gui

import (
	"fmt"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/calvinreu/evosim/src/env"
)

var DEBUG bool

func Init(chart env.Chart, debug bool) (Renderer, error) {
	var renderer Renderer
	renderer.chart = chart
	DEBUG = debug

	err := sdl.Init(sdl.INIT_VIDEO | sdl.INIT_EVENTS | sdl.INIT_TIMER | sdl.INIT_AUDIO)
	if err != nil {
		return renderer, err
	}

	err = ttf.Init()
	if err != nil {
		return renderer, err
	}

	if DEBUG {
		fmt.Println("sdl initialized")
	}

	renderer.window.window, err = sdl.CreateWindow(Title, 0, 0, WindowWidht, WindowHeight, WindowFlags)
	if err != nil {
		return renderer, err
	}

	Wtemp, Htemp := renderer.window.window.GetSize()
	renderer.window.W, renderer.window.H = int(Wtemp), int(Htemp)

	if DEBUG {
		fmt.Println("GUI:Window was initialized")
	}

	renderer.renderer, err = sdl.CreateRenderer(renderer.window.window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		return renderer, err
	}

	//FontPath, err := exec.Command("gui/fontPath.sh", FontName).Output()
	//FontPath = []byte(FontName)
	if err != nil {
		return renderer, err
	}
	renderer.Font, err = ttf.OpenFont(FontName, 20)
	if err != nil {
		return renderer, err
	}

	if DEBUG {
		fmt.Println("GUI:Renderer was initialized")
	}

	renderer.creatureBase.texture, err = img.LoadTexture(renderer.renderer, CreatureTexture)
	if err != nil {
		return renderer, err
	}

	if DEBUG {
		fmt.Println("GUI:Texture was initialized")
	}

	_, _, renderer.creatureBase.sRect.W, renderer.creatureBase.sRect.H, err = renderer.creatureBase.texture.Query()
	if err != nil {
		return renderer, err
	}

	renderer.creatureBase.sRect.X = 0
	renderer.creatureBase.sRect.Y = 0
	renderer.creatureBase.W, renderer.creatureBase.H = float32(renderer.creatureBase.sRect.W), float32(renderer.creatureBase.sRect.H)
	renderer.creatures.Init()
	err = renderer.SetView(0, 0)

	if err != nil {
		return renderer, err
	}

	renderer.creatureBase.center.X = renderer.creatureBase.W / 2
	renderer.creatureBase.center.Y = renderer.creatureBase.H / 2

	if DEBUG {
		fmt.Println("GUI:Init done")
	}

	return renderer, nil
}
