package gui

import (
	"math"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/calvinreu/evosim/src/env"
)

const (
	Title                   = "evosim"
	WindowFlags             = sdl.WINDOW_FULLSCREEN
	CreatureTexture         = "./gui/ressources/creature.png"
	FontName                = "DejaVuSans.ttf"
	FoodColorScalar float64 = 255 / float64(env.MaximumFood)
	TileSize                = 20
	RadianToDegree          = 180 / math.Pi
	WindowHeight            = 1080
	WindowWidht             = 1920
)
