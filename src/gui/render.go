package gui

import (
	"container/list"
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/calvinreu/evosim/src/env"
	"gitlab.com/calvinreu/evosim/src/info"
)

type CreatureGui struct {
	DRect       *sdl.FRect
	Angle, X, Y *float64
}

type Renderer struct {
	Font                   *ttf.Font
	xBeg, xEnd, yBeg, yEnd int
	DEBUG                  bool
	renderer               *sdl.Renderer
	chart                  env.Chart
	creatures              list.List
	creatureBase           struct {
		texture *sdl.Texture
		sRect   sdl.Rect
		center  sdl.FPoint
		W, H    float32
	}
	window struct {
		H, W   int
		window *sdl.Window
	}
}

func (renderer *Renderer) Render(simInfo info.Info) error {
	var tileRect sdl.FRect
	tileRect.X = 0
	tileRect.Y = 0
	tileRect.W = TileSize
	tileRect.H = TileSize
	err := renderer.renderer.SetDrawColor(0, 0, 0, 255)
	if err != nil {
		return err
	}
	err = renderer.renderer.Clear()
	if err != nil {
		return err
	}

	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch e := event.(type) {
		case *sdl.KeyboardEvent:
			key := e.Keysym.Scancode
			switch key {
			case sdl.SCANCODE_UP:
				renderer.MoveView(0, -1)
			case sdl.SCANCODE_DOWN:
				renderer.MoveView(0, 1)
			case sdl.SCANCODE_LEFT:
				renderer.MoveView(-1, 0)
			case sdl.SCANCODE_RIGHT:
				renderer.MoveView(1, 0)
			}
		}
	}

	for _, i := range (renderer.chart.QuickAcces)[int(renderer.yBeg):renderer.yEnd] {
		for _, j := range renderer.chart.Data[renderer.xBeg+i : renderer.xEnd+i] {
			if j.Water {
				err = renderer.renderer.SetDrawColor(0, 0, 255, 255)
			} else {
				err = renderer.renderer.SetDrawColor(255-uint8(j.Food*FoodColorScalar), 255, 140-uint8(j.Food*FoodColorScalar/2), 255)
			}
			if err != nil {
				return err
			}
			err = renderer.renderer.FillRectF(&tileRect)
			if err != nil {
				return err
			}
			tileRect.X += TileSize
		}
		tileRect.X = 0
		tileRect.Y += TileSize
	}

	for i := renderer.creatures.Front(); i != nil; i = i.Next() {
		if creature, ok := i.Value.(CreatureGui); ok {
			creature.DRect.X = (float32(*creature.X)-float32(renderer.xBeg))*TileSize - creature.DRect.W/2
			creature.DRect.Y = (float32(*creature.Y)-float32(renderer.yBeg))*TileSize - creature.DRect.H/2
			err = renderer.renderer.CopyExF(renderer.creatureBase.texture, &renderer.creatureBase.sRect, creature.DRect, *creature.Angle*RadianToDegree, &renderer.creatureBase.center, sdl.FLIP_NONE)
			if err != nil {
				return err
			}
		} else if i.Value != nil {
			return fmt.Errorf("gui: creatures list does not only contain CreatureGui object type is %v", i.Value)
		}

	}

	err = renderer.DrawText(simInfo)
	if err != nil {
		return err
	}

	renderer.renderer.Present()

	if DEBUG {
		fmt.Println("GUI:Renderer:Render done")
	}

	return nil
}

func (renderer *Renderer) Quit() {
	var j *list.Element
	for i := renderer.creatures.Front(); i != nil; i = j {
		j = i.Next()
		renderer.creatures.Remove(i)
	}
	renderer.creatureBase.texture.Destroy()
	renderer.renderer.Destroy()
	renderer.window.window.Destroy()

	renderer.Font.Close()
	ttf.Quit()
	sdl.Quit()
}
