package gui

import (
	"fmt"
)

func (renderer *Renderer) MoveView(x, y int) error {

	//dont move out of the map in negative direction
	if renderer.xBeg+x < 0 {
		x = 0 - renderer.xBeg
	}
	if renderer.yBeg+y < 0 {
		y = 0 - renderer.yBeg
	}

	renderer.xBeg += x
	renderer.yBeg += y
	renderer.xEnd = renderer.xBeg + renderer.window.W/TileSize + 1
	renderer.yEnd = renderer.yBeg + renderer.window.H/TileSize + 1

	if renderer.xEnd >= renderer.chart.W {
		renderer.xEnd = renderer.chart.W - 1
		renderer.xBeg = renderer.xEnd - renderer.window.W/TileSize
		if renderer.xBeg < 0 {
			renderer.xBeg = 0
		}
	}

	if renderer.yEnd >= renderer.chart.H {
		renderer.yEnd = renderer.chart.H - 1
		renderer.yBeg = renderer.yEnd - renderer.window.H/TileSize
		if renderer.yBeg < 0 {
			renderer.yBeg = 0
		}
	}

	/* done in render function
	for i := renderer.creatures.Front(); i != nil; i = i.Next() {
		if creature, ok := i.Value.(CreatureGui); ok {
			creature.DRect.X += float32(x) / TileSize
			creature.DRect.Y += float32(y) / TileSize
		} else {
			return fmt.Errorf("gui: creatures list does not only contain CreatureGui object type is %v", i.Value)
		}
	}*/
	return nil
}

func (renderer *Renderer) SetView(x, y int) error {

	//dont move out of the map in negative direction
	if x < 0 {
		return fmt.Errorf("x: %v value out of range", x)
	}
	if y < 0 {
		return fmt.Errorf("y: %v value out of range", y)
	}
	/*done in render function
	for i := renderer.creatures.Front(); i != nil; i = i.Next() {
		if creature, ok := i.Value.(CreatureGui); ok {
			creature.DRect.X = float32(x-renderer.xBeg) / TileSize
			creature.DRect.Y = float32(y-renderer.yBeg) / TileSize
		} else {
			return errors.New("gui: creatures list does not only contain CreatureGui object")
		}
	}
	*/

	renderer.xBeg = x
	renderer.yBeg = y
	renderer.xEnd = renderer.xBeg + renderer.window.W/TileSize + 1
	renderer.yEnd = renderer.yBeg + renderer.window.H/TileSize + 1

	if renderer.xEnd >= renderer.chart.W {
		renderer.xEnd = renderer.chart.W - 1
		renderer.xBeg = renderer.xEnd - renderer.window.W/TileSize
		if renderer.xBeg < 0 {
			renderer.xBeg = 0
		}
	}

	if renderer.yEnd >= renderer.chart.H {
		renderer.yEnd = renderer.chart.H - 1
		renderer.yBeg = renderer.yEnd - renderer.window.H/TileSize
		if renderer.yBeg < 0 {
			renderer.yBeg = 0
		}
	}

	return nil
}
