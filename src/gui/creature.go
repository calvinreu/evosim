package gui

import (
	"container/list"
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

func (renderer *Renderer) RemoveCreature(creature *list.Element) error {

	if creature == nil {
		return fmt.Errorf("creature to remove is nil")
	}

	v := renderer.creatures.Remove(creature)
	if v == nil {
		return fmt.Errorf("unable to remove creature from renderer")
	}

	if DEBUG {
		fmt.Println("GUI:creature removed")
	}

	return nil
}

func (renderer *Renderer) AddCreature(x, y, angle *float64) *list.Element {
	var creature CreatureGui
	var dRect sdl.FRect
	creature.DRect = &dRect
	creature.X = x
	creature.Y = y
	creature.Angle = angle
	creature.DRect.X = float32(*x)
	creature.DRect.Y = float32(*y)
	creature.DRect.W = renderer.creatureBase.W
	creature.DRect.H = renderer.creatureBase.H

	if DEBUG {
		fmt.Println("GUI:Renderer:AddCreature done")
	}

	v := renderer.creatures.PushFront(creature)
	if v == nil {
		fmt.Println("Fatal error unable to add creature")
	}
	return v
}
