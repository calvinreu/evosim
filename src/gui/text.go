package gui

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/calvinreu/evosim/src/info"
)

func (renderer *Renderer) DrawText(simInfo info.Info) error {
	var color sdl.Color
	color.A = 255
	color.R = 255
	color.G = 255
	color.B = 255
	var textRect sdl.Rect

	textSurface, err := renderer.Font.RenderUTF8Solid(simInfo.Format(), color)
	if err != nil {
		return err
	}

	textTexture, err := renderer.renderer.CreateTextureFromSurface(textSurface)
	if err != nil {
		return err
	}

	_, _, textRect.W, textRect.H, err = textTexture.Query()
	if err != nil {
		return err
	}
	err = renderer.renderer.Copy(textTexture, &textRect, &textRect)
	if err != nil {
		return err
	}

	return nil
}
