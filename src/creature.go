package main

import (
	"container/list"
	"fmt"
	"math"
	"math/rand"

	"gitlab.com/calvinreu/evosim/src/nn"
)

type Creature struct {
	food, x, y, angle, age float64
	timer                  int
	self, guiInstance      *list.Element
	nnet                   nn.NeuralNetwork
}

func CreatureInit(creatureData CreatureData) {
	var creature Creature
	creature.angle = creatureData.Angle
	creature.food = creatureData.Food
	creature.x, creature.y = creatureData.X, creatureData.Y
	creature.nnet.SetData(creatureData.NN)
	creature.self = creatures.PushFront(&creature)
}

func NewCreature() {
	var creature Creature
	creature.x = rand.Float64() * (float64(chart.W) - 1)
	creature.y = rand.Float64() * (float64(chart.H) - 1)
	creature.food = FirstCreatureEnergy
	creature.angle = rand.Float64() * math.Pi * 2

	creature.self = creatures.PushFront(&creature)

	chart.At(int(creature.x), int(creature.y)).CreatureC++

	creature.nnet.Init(LayerSize())
	creature.nnet.FillRand()

	if GuiEnabled {
		creature.guiInstance = renderer.AddCreature(&creature.x, &creature.y, &creature.angle)
	}

	if DEBUG {
		fmt.Println(creature)
	}
}

func (creature *Creature) Refresh() {
	output := creature.nnet.GetOutput(creature.GetInput())

	if creature.timer > 0 {
		creature.timer--
	}

	creatureAlive := creature.Move(output[1])
	if output[0] > AktiveOutputThreshhold && creatureAlive {
		creatureAlive = creature.Eat()
	}
	if !creatureAlive {
		return
	}
	creatureAlive = creature.Turn(output[2])
    creature.angle -= math.Pi * 2 * math.Floor(creature.angle / (math.Pi * 2));
	if output[3] > AktiveOutputThreshhold && creatureAlive {
		creatureAlive = creature.Procreate()
	}
	if !creatureAlive {
		return
	}
	creature.food -= BaseFoodConsumption + math.Sqrt(creature.age)
	if chart.At(int(creature.x), int(creature.y)).Water {
		creature.food -= WaterPenalty
	}
	creature.CheckFood()
}

func (creature *Creature) CheckFood() bool {
	if creature.food < 0 {
		err := creature.Destroy()
		if err != nil {
			fmt.Println(err)
		}
		return false
	}
	return true
}
