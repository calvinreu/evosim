package main

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/calvinreu/evosim/src/env"
	"gitlab.com/calvinreu/evosim/src/info"
	"gitlab.com/calvinreu/evosim/src/nn"
)

type SaveData struct {
	Chart     env.Chart
	Creatures []CreatureData
	SimInfo   info.Info
}

type CreatureData struct {
	Food, X, Y, Angle float64
	NN                nn.NNData
}

func Save(filename string) {
	var data []byte
	var saveData SaveData
	saveData.SimInfo = simInfo
	saveData.Chart = chart
	saveData.Creatures = make([]CreatureData, creatures.Len())
	for e, i := creatures.Front(), 0; e != nil; e = e.Next() {
		if creature, ok := e.Value.(*Creature); ok {
			saveData.Creatures[i].X = creature.x
			saveData.Creatures[i].Y = creature.y
			saveData.Creatures[i].Angle = creature.angle
			saveData.Creatures[i].Food = creature.food
			saveData.Creatures[i].NN = creature.nnet.GetData()
		}
		i++
	}
	file, err := os.Create(filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	data, err = json.Marshal(saveData)
	if err != nil {
		fmt.Println(err)
		return
	}
	var n int
	n, err = file.Write(data)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n, " bytes writen to file", filename)
}
