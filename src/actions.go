package main

import (
	"fmt"
	"math"
)

func (creature *Creature) Eat() bool {
	if chart.At(int(creature.x), int(creature.y)).Food > FoodPerEat {
		creature.food += FoodPerEat
		chart.At(int(creature.x), int(creature.y)).Food -= FoodPerEat
	} else {
		creature.food += chart.At(int(creature.x), int(creature.y)).Food
		chart.At(int(creature.x), int(creature.y)).Food = 0
	}
	creature.food -= ActionEatEnergy
	return creature.CheckFood()
}

func (creature *Creature) Move(speed float64) bool {

	chart.At(int(creature.x), int(creature.y)).CreatureC--

	if speed < 0 {
		creature.x += math.Cos(creature.angle) * speed * SpeedScalar * BackwardsMovementScalar
		creature.y += math.Sin(creature.angle) * speed * SpeedScalar * BackwardsMovementScalar

	} else {
		creature.x += math.Cos(creature.angle) * speed * SpeedScalar
		creature.y += math.Sin(creature.angle) * speed * SpeedScalar
	}

	creature.food -= speed * speed * MovementEnergyScalar

	if creature.x >= float64(chart.W) {
		creature.x -= float64(chart.W)
	}

	if creature.y >= float64(chart.H) {
		creature.y -= float64(chart.H)
	}

	if creature.x < 0 {
		creature.x = float64(chart.W) + creature.x - 1
	}

	if creature.y < 0 {
		creature.y = float64(chart.H) + creature.y - 1
	}

	chart.At(int(creature.x), int(creature.y)).CreatureC++

	return creature.CheckFood()

}

func (creature *Creature) Turn(angle float64) bool {
	creature.angle += angle
	creature.food -= TurnEnergyScalar * angle * angle
	return creature.CheckFood()
}

func (creature *Creature) Procreate() bool {
	var child Creature
	if creature.timer != 0 {
		return true
	}
	creature.food -= ProcreateEnergy
	creature.timer = 20
	if creature.CheckFood() {
		child.food = NewCreatureEnergy
		child.x, child.y, child.angle = creature.x, creature.y, creature.angle
		child.timer = 50
		child.self = creatures.PushFront(&child)

		child.nnet = creature.nnet.Mutate()
		chart.At(int(creature.x), int(creature.y)).CreatureC++

		if GuiEnabled {
			child.guiInstance = renderer.AddCreature(&child.x, &child.y, &child.angle)
		}
		return true
	}
	return false
}

func (creature *Creature) Destroy() error {
	chart.At(int(creature.x), int(creature.y)).CreatureC--
	v := creatures.Remove(creature.self)
	if v == nil {
		return fmt.Errorf("unable to remove creature from creatures")
	}
	if GuiEnabled {
		return renderer.RemoveCreature(creature.guiInstance)
	}

	return nil
}
