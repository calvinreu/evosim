package main

import (
	"container/list"
	"fmt"
	"strings"
	"time"
)

func Mainloop() {
	command := make(chan string)
	go GetInputString(command)

	for {
		select {
		case c := <-command:
			args := strings.SplitAfter(c, "--")
			for i := 0; i < len(args)-1; i++ {
				args[i] = strings.ReplaceAll(args[i], "-", "")
			}
			switch args[0] {
			case "exit":
				return
			case "save":
				var filename string
				if len(args) < 2 {
					filename = "savefile.json"
				} else {
					filename = args[1]
				}
				Save(filename)
				go GetInputString(command)
			case "disable_gui":
				GuiEnabled = false
				renderer.Quit()
				go GetInputString(command)
			case "enable_gui":
				EnableGui()
				GuiEnabled = true
				go GetInputString(command)
			case "creature_count":
				fmt.Println(creatures.Len())
				go GetInputString(command)
			case "ticks":
				fmt.Println(simInfo.Ticks)
				go GetInputString(command)
			default:
				fmt.Println("unkown command")
				go GetInputString(command)
			}
		default:
			beginTime := time.Now()
			var temp *list.Element
			for j := creatures.Front(); j != nil; j = temp {
				temp = j.Next()
				if creature, ok := j.Value.(*Creature); ok {
					creature.Refresh()
				}
			}
			if creatures.Len() < StartCreature {
				for i := creatures.Len(); i < StartCreature; i++ {
					NewCreature()
				}
			}
			chart.Refresh()
			if GuiEnabled {
				err := renderer.Render(simInfo)
				if err != nil {
					fmt.Println(err)
				}
				if time.Now().UnixMilli()-beginTime.UnixMilli() < TickTime {
					time.Sleep(time.Duration((TickTime - time.Now().UnixNano() + time.Now().UnixNano())))
				}
			}

			simInfo.CreatureCount = uint(creatures.Len())
			simInfo.Ticks++
		}
	}
}
