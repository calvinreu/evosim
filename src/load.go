package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func Load(filename string) {
	var saveData SaveData
	data, err := os.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(data, &saveData)
	if err != nil {
		fmt.Println(err)
	}

	chart = saveData.Chart
	simInfo = saveData.SimInfo
	for _, i := range saveData.Creatures {
		CreatureInit(i)
		if DEBUG {
			fmt.Println("load creature")
		}
	}
}
