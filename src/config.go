package main

import "time"

const (
	MapWidth, MapHeight             = 200, 200
	StartCreature                   = 100
	FoodPerEat                      = 0.7
	ActionEatEnergy                 = 0.2
	SpeedScalar             float64 = 1
	MovementEnergyScalar    float64 = 4
	TurnEnergyScalar                = 0.1
	ProcreateEnergy                 = 10
	NewCreatureEnergy               = 5
	InputSize                       = 29
	OutputSize                      = 4
	AktiveOutputThreshhold          = 0
	ProcreateTimer                  = 20
	AgePerTick                      = 0.02
	BaseFoodConsumption             = 0.1
	FirstCreatureEnergy             = 5
	TickTime                        = 40 * int64(time.Millisecond/time.Nanosecond)
	WaterPenalty                    = 0.5
	BackwardsMovementScalar         = 0.7
)

func LayerSize() []int {
	return []int{InputSize, 20, 10, OutputSize}
}
