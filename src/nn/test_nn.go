package nn

import "fmt"

func Test() bool {
	var nn NeuralNetwork
	input := make([]int, 2)
	output := make([][]float64, 10)
	input[0] = 20
	input[1] = 5
	nn.Init(input)
	nn.FillRand()
	in := make([]float64, 20)
	for n := 0; n < 10; n++ {
		for i := 0; i < len(in); i++ {
			in[i] = float64(n)
		}
		output[n] = nn.GetOutput(in)
	}

	fmt.Println(output[0])
	fmt.Println(output[1])
	for c := 0; c < len(output)-1; c++ {
		for j := 0; j < len(output[c]); j++ {
			if output[c][j] == output[c+1][j] {
				fmt.Print(c)
				fmt.Print(":")
				fmt.Println(j)
				return false
			}
		}
	}
	return true
}
