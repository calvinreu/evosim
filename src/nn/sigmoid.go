package nn

import "math"

func SigmoidVec(arguments []float64) Vector {
	out := make([]float64, len(arguments))
	for i := 0; i < len(arguments); i++ {
		out[i] = Sigmoid(arguments[i])
	}
	return out
}

func Sigmoid(x float64) float64 {
	return 1 / (1 + math.Exp(-x))
}
