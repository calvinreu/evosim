package nn

import "fmt"

type Mat [][]float64

func (mat *Mat) Init(cols, rows int) {
	*mat = make(Mat, cols)
	for i := 0; i < cols; i++ {
		(*mat)[i] = make([]float64, rows)
	}
}

func (mat *Mat) Multiply(vec []float64) Vector {
	result := make([]float64, len(*mat))
	if len((*mat)[0]) != len(vec) {
		fmt.Println("len of mat unequal to len of vec: ", len(vec))
	}

	for i := 0; i < len(*mat); i++ {
		for j := 0; j < len(*mat); j++ {
			result[i] += (*mat)[i][j] * vec[j]
		}
	}
	return result
}
