package nn

import "fmt"

type Vector []float64

func (vec1 Vector) Add(vec2 Vector) Vector {
	if len(vec1) != len(vec2) {
		fmt.Println("vector mismatch size")
		return nil
	}
	vec3 := make(Vector, len(vec1))
	for i, v := range vec1 {
		vec3[i] = v + vec2[i]
	}
	return vec3
}
