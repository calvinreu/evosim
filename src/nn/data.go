package nn

type NNData struct {
	Weights []Mat
	Bias    []Vector
}

func (nn *NeuralNetwork) GetData() NNData {
	var nnd NNData
	nnd.Weights = nn.weights
	nnd.Bias = nn.bias

	return nnd
}

func (nn *NeuralNetwork) SetData(nnd NNData) {
	nn.weights = nnd.Weights
	nn.bias = nnd.Bias
}

func (nn *NeuralNetwork) GetWeights() []Mat {
	return nn.weights
}

func (nn *NeuralNetwork) GetBias() []Vector {
	return nn.bias
}

func (nn *NeuralNetwork) SetBias(bias []Vector) {
	nn.bias = bias
}

func (nn *NeuralNetwork) SetWeights(weights []Mat) {
	nn.weights = weights
}
