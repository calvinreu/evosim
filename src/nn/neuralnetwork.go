package nn

import (
	"math/rand"
)

type NeuralNetwork struct {
	weights []Mat
	bias    []Vector
}

func (nn *NeuralNetwork) Init(layersize []int) {
	nn.weights = make([]Mat, len(layersize)-1)
	for i := 0; i < len(layersize)-1; i++ {
		nn.weights[i].Init(layersize[i+1], layersize[i])
	}
	nn.bias = make([]Vector, len(layersize))
	for i := 0; i < len(layersize); i++ {
		nn.bias[i] = make(Vector, layersize[i])
	}
}

func (nn *NeuralNetwork) FillRand() {
	for l := 0; l < len(nn.weights); l++ {
		for col := 0; col < len(nn.weights[l]); col++ {
			for row := 0; row < len(nn.weights[l][0]); row++ {
				nn.weights[l][col][row] = rand.Float64() - 0.5
			}
		}
	}
	for i := 0; i < len(nn.bias); i++ {
		for j := 0; j < len(nn.bias[i]); j++ {
			nn.bias[i][j] = -rand.Float64()
		}
	}

}

func (nn *NeuralNetwork) GetOutput(input Vector) []float64 {
	out := nn.weights[0].Multiply(input.Add(nn.bias[0])).Add(nn.bias[1])

	for c, i := range nn.weights[1:] {
		out = SigmoidVec(i.Multiply(out)).Add(nn.bias[c+2])
	}

	return out
}

func (nn *NeuralNetwork) Mutate() NeuralNetwork {
	var child NeuralNetwork

	child.weights = make([]Mat, len(nn.weights))

	for l := 0; l < len(nn.weights); l++ {
		child.weights[l] = make(Mat, len(nn.weights[l]))
		for col := 0; col < len(nn.weights[l]); col++ {
			child.weights[l][col] = make([]float64, len(nn.weights[l][0]))
			for row := 0; row < len(nn.weights[l][0]); row++ {
				child.weights[l][col][row] = nn.weights[l][col][row] + (rand.Float64()-0.5)*MutationRate
			}
		}
	}
	child.bias = make([]Vector, len(nn.bias))
	for i := 0; i < len(nn.bias); i++ {
		child.bias[i] = make(Vector, len(nn.bias[i]))
		for j := 0; j < len(nn.bias[i]); j++ {
			child.bias[i][j] = nn.bias[i][j] + (rand.Float64()-0.5)*MutationRate
		}
	}

	return child
}
